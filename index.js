console.log("hi");

let posts = [];

let count = 1; // will serve as the post id

//add post data
document.querySelector("#form-add-post").addEventListener("submit", (e)=>{

	e.preventDefault(); //prevent the page from loading, hinders the default behavior of the event
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	count++; //increment the id number
	showPosts(posts);
	alert("Movie successfully added");
	console.log(posts);
});

//show posts
const showPosts = (posts) =>{
	console.log(posts);
	let postsEntries = ''; //will catch each post

	posts.forEach((post)=>{
		console.log(post);
		postsEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div> 
		`;//to concatenate or link the content of div
	});
	document.querySelector('#div-post-entries').innerHTML = postsEntries;
};

//edit posts
const editPost = (id)=>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;


	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};

//update post
document.querySelector("#form-edit-post").addEventListener('submit', (e) =>{
	e.preventDefault();
	for(let i=0; i < posts.length; i++){
	//i represent for index
		if (posts[i].id.toString()===document.querySelector('#txt-edit-id').value){
		//.toString to change the number value to string to be equal with the querySelector
			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPosts(posts);
			alert('Movie post successfully updated');

			break;
			//to prevent the loop in updating all other posts
		}
	}
})


const deletePost = (id)=>{
	posts.pop({
		title: document.querySelector(`#post-title-${id}`).innerHTML,
		body: document.querySelector(`#post-title-${id}`).innerHTML
	});
	showPosts(posts);
	alert("Movie successfully removed");
	console.log(posts);
};

